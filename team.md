---
layout: left_col
title: Team
permalink: /about-node/team
section: About
wider: true
pages:
  - title: ELIXIR-LU Team
    href: /about-node/team
    active: True
  - title: Affiliated collaborators
    href: /about-node/affiliated_collaborators      
  - title: Collaborations
    href: /about-node/collaborations
  - title: Funding and governance
    href: /about-node/funding-and-governance
  - title: Vacancies
    href: /about-node/vacancies
---

<h1><marquee>This is just a preview! The content is not reflected from the original webpage</marquee></h1>

<div class="cards">

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Reinhard Schneider" />
    </div>
    <div class="card-id-name">
      <a href="http://wwwen.uni.lu/lcsb/people/reinhard_schneider">Reinhard Schneider</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Head of Node (HoN)
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Wei Gu" />
    </div>
    <div class="card-id-name">
      <a href="http://wwwen.uni.lu/lcsb/people/wei_gu">Wei Gu</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Deputy HoN, Scientific Coordinator
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Regina Becker" />
    </div>
    <div class="card-id-name">
      <a href="https://wwwen.uni.lu/lcsb/people/regina_becker">Regina Becker</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Strategy Development Advisor
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Venkata Satagopam" />
    </div>
    <div class="card-id-name">
      <a href="http://wwwen.uni.lu/lcsb/people/venkata_satagopam">Venkata Satagopam</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Technical Coordinator
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Christophe Trefois" />
    </div>
    <div class="card-id-name">
      <a href="http://wwwen.uni.lu/lcsb/people/christophe_trefois">Christophe Trefois</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Deputy Technical Coordinator
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Roland Krause" />
    </div>
    <div class="card-id-name">
      <a href="http://wwwen.uni.lu/lcsb/people/roland_krause">Roland Krause</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Training Coordinator
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Veronica Codoni" />
    </div>
    <div class="card-id-name">
      <a href="https://wwwen.uni.lu/lcsb/people/veronica_codoni">Veronica Codoni</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Deputy Training Coordinator
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Pinar Alper" />
    </div>
    <div class="card-id-name">
      <a href="https://wwwen.uni.lu/lcsb/people/pinar_alper">Pinar Alper</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Data Steward
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Marek Ostaszewski" />
    </div>
    <div class="card-id-name">
      <a href="https://wwwen.uni.lu/lcsb/people/marek_ostaszewski">Marek Ostaszewski</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Disease Maps Service Coordinator
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Noua Toukourou" />
    </div>
    <div class="card-id-name">
      <a href="http://wwwen.uni.lu/lcsb/people/noua_toukourou">Noua Toukourou</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        System Administrator
      </small>
    </div>
  </div>

  <div class="card-id smaller">
    <div class="card-id-smallphoto">
      <img src="{{ '/assets/photos/placeholder.png' | relative_url }}" title="Jacek Lebioda" />
    </div>
    <div class="card-id-name">
      <a href="https://wwwen.uni.lu/lcsb/people/jacek_jaroslaw_lebioda">Jacek Lebioda</a>
    </div>
    <div class="card-id-description">
      <small style="font-size: 11.5px;">
        Software Engineer
      </small>
    </div>
  </div>

</div>  
