---
layout: left_col
title: Funding and governance
permalink: /about-node/funding-and-governance
section: About
wider: true
pages:
  - title: ELIXIR-LU Team
    href: /about-node/team
  - title: Affiliated collaborators
    href: /about-node/affiliated_collaborators    
  - title: Collaborations
    href: /about-node/collaborations
  - title: Funding and governance
    href: /about-node/funding-and-governance
    active: True
  - title: Vacancies
    href: /about-node/vacancies
---

<h1><marquee>This is just a preview! The content is not reflected from the original webpage</marquee></h1>

ELIXIR-LU is supported by the **Luxembourg Ministry of Higher Education and Research** (MESR) and the **Luxembourg Centre for Systems Biomedicine** (LCSB) through direct and in-kind contributions.

Their joint support provides the computational and personnel infrastructure necessary for offering sustainable services to the national and international research community.

---

![MESR logo]({{ '/assets/logos/mesr_en-hdpi.png' | relative_url }}){:height="100px" style="padding-right: 80px"} ![uni.lu and LCSB logos]({{ '/assets/logos/logo_LCSB_UL.png' | relative_url }}){:height="100px"}

---

## The Scientific Advisory Board
The Scientific Advisory Board is composed of seven members:

<div class="cards">

  <div class="card-id card-id-sab">
    <div class="card-id-photo">
      <img src="{{ '/assets/photos/placeholder2.png' | relative_url }}" title="Dr. Antonio Andreu" />
    </div>
    <div class="card-id-name">
      Dr. Antonio Andreu<hr/>
    </div>
    <div class="card-id-description">
      Scientific Director of EATRIS <br /><small>(the European Infrastructure for Translational Medicine)</small>.
    </div>
  </div>

  <div class="card-id card-id-sab">
    <div class="card-id-photo">
      <img src="{{ '/assets/photos/placeholder2.png' | relative_url }}" title="Prof. Ron Appel" />
    </div>
    <div class="card-id-name">
      Prof. Ron Appel<hr/>
    </div>
    <div class="card-id-description">
      ELIXIR-CH Head of Node
      <br />Executive Director of the SIB <small>(Swiss Institute of Bioinformatics)</small>
      <br />
      Professor at the University of Geneva.
    </div>
  </div>

  <div class="card-id card-id-sab">
    <div class="card-id-photo">
      <img src="{{ '/assets/photos/placeholder2.png' | relative_url }}" title="Prof. Søren Brunak" />
    </div>
    <div class="card-id-name">
      Prof. Søren Brunak<hr/>
    </div>
    <div class="card-id-description">
      ELIXIR-DK Head of Node<br />
      Research Director at the Novo Nordisk Foundation Center for Protein Research.<br />
      Professor at the University of Copenhagen and the Technical University of Denmark.
    </div>
  </div>

  <div class="card-id card-id-sab">
    <div class="card-id-photo">
      <img src="{{ '/assets/photos/placeholder2.png' | relative_url }}" title="Dr. Jacques Demotes-Maynard" />
    </div>
    <div class="card-id-name">
      Dr. Jacques Demotes-Maynard<hr/>
    </div>
    <div class="card-id-description">
      Director of ECRIN <br /><small>(the European Clinical Research Infrastructure Network)</small>.
    </div>
  </div>

  <div class="card-id card-id-sab">
    <div class="card-id-photo">
      <img src="{{ '/assets/photos/placeholder2.png' | relative_url }}" title="Dr. Susan E. Wallace" />
    </div>
    <div class="card-id-name">
      Dr. Susan E. Wallace<hr/>
    </div>
    <div class="card-id-description">
      ELSI expert.<br />Lecturer of Population and Public Health Sciences, University of Leicester.
    </div>
  </div>

  <div class="card-id card-id-sab">
    <div class="card-id-photo">
      <img src="{{ '/assets/photos/placeholder2.png' | relative_url }}" title="Erik Steinfelder" />
    </div>
    <div class="card-id-name">
      Erik Steinfelder<hr/>
    </div>
    <div class="card-id-description">
      Industry advisor. <br />Biobanking Market Development Director, Thermo Fisher Scientific.
    </div>
  </div>

  <div class="card-id card-id-sab">
    <div class="card-id-photo">
      <img src="{{ '/assets/photos/placeholder2.png' | relative_url }}" title="Michaela Th. Mayrhofer" />
    </div>
    <div class="card-id-name">
      Michaela Th. Mayrhofer<hr/>
    </div>
    <div class="card-id-description">
      Interim Co-director General of BBMIR-ERIC<br /><small>(the Biobanking and Biomolecular resources Research Infrastructure – European Research Infrastructure Consortium)</small>. 
    </div>
  </div>

</div>

Additionally, a **National Stakeholder Committee** has been formed. It is composed of representatives from the *Ministry of Higher Education and Research*, and all government and research institutions with interests in the biomedical "ecosystem" in Luxembourg. Input from this committee helps ELIXIR-LU align with the interests of our national data provider and user communities.
